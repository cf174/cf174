#include <stdio.h>

int main()
{   float x, y;
    float *a, *b;
    float sum, diff, mult, div;

    a = &x;
    b = &y;

    printf("Enter any two numbers: ");
    scanf("%f %f", a, b);

    sum  = (*a) + (*b);
    diff = (*a) - (*b);
    mult = (*a) * (*b);
    div  = (*a) / (*b);

    printf("Sum = %f \n", sum);
    printf("Difference = %f \n", diff);
    printf("Product = %f \n", mult);
    printf("Quotient = %f \n", div);
    return 0;
}