#include <stdio.h>
struct book
{
    char title[40];
    char author[30];
    float price;
    int numberofpages;

}b1,b2;
int main()
{
        printf("Enter the title,author,price and number of pages of book 1: \n");
        scanf("%s %s %f %d",b1.title,b1.author,&b1.price,&b1.numberofpages);

        printf("Enter the title,author,price and number of pages of book 2: \n");
        scanf("%s %s %f %d",b2.title,b2.author,&b2.price,&b2.numberofpages);

        if(b1.price > b2.price)
        {
            printf("%s is expensive than %s \n",b1.title,b2.title);
        }
        else
        {
            printf("%s is expensive than %s \n",b2.title,b1.title);
        }

        if(b1.numberofpages < b2.numberofpages)
        {
            printf("The Author of the book having less number of pages is %s",b1.author);
        }
        else
        {
            printf("The Author of the book having less number of pages is %s",b2.author);
        }

        return 0;
    }
