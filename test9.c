#include <stdio.h>
struct student
{
    int rollnumber;
    char name[20];
    char section[20];
    char department[30];
    float fees;
    float results;
} S1,S2;

int main()
{

    printf("Enter the roll number,name,section,department,fees,results of student 1: ");
    scanf("%d %s %s %s %f %f",&S1.rollnumber,S1.name,S1.section,S1.department,&S1.fees,&S1.results);

    printf("Enter the roll number,name,section,department,fees,results of student 2: ");
    scanf("%d %s %s %s %f %f",&S2.rollnumber,S2.name,S2.section,S2.department,&S2.fees,&S2.results);

if(S1.results > S2.results)
    printf("Student 1 scored the highest");
else
    printf("Student 2 scored the highest");
return 0;
}

